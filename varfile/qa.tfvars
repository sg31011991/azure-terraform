resource_group_name  = "storage_terraform_rg_qa"
location             = "West Europe"
storage_account_name = "storageacc017sg9"
account_kind         = "StorageV2"
account_tier          = "Standard"
tags = {
  Owner = "SG"
  env   = "qa"

}
# Container lists with access_type to create
  containers_list = [
    { name = "mystore250", access_type = "private" },
    { name = "blobstore251", access_type = "blob" },
    { name = "containter252", access_type = "container" }
  ]